import express, { Router, Request, Response, NextFunction } from 'express';
import { UserServices } from '../../services/UserServices';

const router: Router = express.Router();

export function userRoute (userServices: UserServices): Router {

    router.get('/', async (req: Request, res: Response, next: NextFunction) => {
        const users = await userServices.getAllUsers();
        res.json(users);
    })

    router.get('/get', async (req: Request, res: Response, next: NextFunction ) => {
        const user = await userServices.getUser(req.query.user_id)
        res.json(user)
    })

    router.post('/add', async (req: Request, res: Response, next: NextFunction) => {
        const { name , age, phone } = req.body;
        const user = await userServices.addUser(name, age, phone);
        res.json(user);
    })

    return router;
}