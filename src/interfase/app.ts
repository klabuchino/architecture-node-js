import express, { Application, Router} from 'express';
import { userRoute } from './routes/user';


export class App {
    private readonly app: Application;

    constructor() {
        this.app = express();
        this.app.use(express.json());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
    }

    public appFactory(allServices: any) {
        const userRouter: Router = userRoute(allServices.UserServices);

        this.app.use('/user', userRouter);
        
        return this.app;
    }

}