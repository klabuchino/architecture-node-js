import { IUser } from '../domain/user/IUser';
import { IUserRepository } from '../domain/user/IUserRepository';

export class UserServices {

    private readonly userRepository: IUserRepository;

    constructor(userRepository: IUserRepository) {
        this.userRepository = userRepository;
    }

    async getUser(id: string): Promise<IUser | null> {
        const user = await this.userRepository.get(id);
        return user;
    }

    async getAllUsers() {
        const users = await this.userRepository.getAll();
        return users
    }

    public async addUser(name: string, age: number, phone: string): Promise<IUser | string> {
        try {
            const user = await this.userRepository.add(name, age, phone);
            return user;
        } catch(error) {
            return 'error add user: ' + error.message
        }
    }
}