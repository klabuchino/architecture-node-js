import { IUser } from './IUser';


export interface IUserRepository {
    get(id: string): Promise<IUser | null>
    getAll(): Promise<IUser[]>
    add(name: string, age: number, phone: string): Promise<IUser> 
}