import mongoose from 'mongoose';

export class Database {
    private readonly connectionString: string;

    constructor( connectionString: string ) {
        this.connectionString = connectionString;
    }

    async connect() {
        try {
            await mongoose.connect(this.connectionString, 
                { 
                    useNewUrlParser: true, 
                    useUnifiedTopology: true 
                } )
            console.log('Database successfully connected. URL::: ' + this.connectionString)
        } catch (error) {
            console.log('Database connection failed ::: ' + error.message)
            throw (error);
        }
    }
}