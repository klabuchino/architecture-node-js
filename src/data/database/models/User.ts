import mongoose, { Schema, Document } from 'mongoose';
import { IUser } from '../../../domain/user/IUser';

export interface IUserModel extends IUser, Document {
    toUser(): IUser;
}


const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    }
})



export const UserModel = mongoose.model<IUserModel>("User", schema);