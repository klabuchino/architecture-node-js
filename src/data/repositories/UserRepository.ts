import { IUserRepository } from '../../domain/user/IUserRepository';
import { IUser } from '../../domain/user/IUser';
import { UserModel, IUserModel } from '../database/models/User';

export class UserRepository implements IUserRepository {

    async get(id: string): Promise<IUser | null> {
        const user = await UserModel.findById(id);
        if(user) {
            return user;
        }
        return null
    }

    async getAll(): Promise<IUser[]> {
        const users = await UserModel.find();
        return users;
    }

    async add(name: string, age: number, phone: string): Promise<IUser> {
        let newUser = new UserModel( { name, age, phone } );
        newUser = await newUser.save();
        return newUser;
    }

}