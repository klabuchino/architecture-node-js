import { config } from './config';
import { App } from './interfase/app';
import { allRepositories } from './data/repositories/allRepositories';
import { allServices } from './services/allServices';
import { Database } from './data/database/index';

const database = new Database(config.connectionString as string);
database.connect();

const repositories = allRepositories();
const services = allServices(repositories);


const app = new App().appFactory(services);

app.listen(config.port, () => {
    console.log(`server running on ${config.port}`)
})
